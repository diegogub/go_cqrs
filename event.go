package go_cqrs

import (
	"encoding/json"
	"github.com/rs/xid"
	"strings"
	"time"
)

type BaseEvent struct {
	ID          string    `json:"i"`
	Type        string    `json:"t"`
	Stream      string    `json:"s"`
	EventsLinks []string  `json:"l"`
	Timestamp   time.Time `json:"u"`
}

type Event struct {
	BaseEvent
	Data map[string]interface{} `json:"d"`
}

// creates new event
func NewEvent(id, eventType, stream string, data interface{}) (Event, error) {
	var event Event
	if id == "" {
		event.ID = xid.New().String()
	} else {
		event.ID = id
	}

	event.Stream = strings.ToLower(stream)
	event.Type = strings.ToLower(eventType)

	tdata := make(map[string]interface{})
	b, err := json.Marshal(data)
	if err != nil {
		return event, err
	}

	err = json.Unmarshal(b, &tdata)
	if err != nil {
		return event, err
	}

	event.Data = tdata

	return event, err
}
